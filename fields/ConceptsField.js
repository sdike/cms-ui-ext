define(function(require, exports, module) {

    var $ = require("jquery");

    var Alpaca = $.alpaca;
    
    if (!Alpaca.Fields.WWNorton) {
        Alpaca.Fields.WWNorton = {};
    }

    Alpaca.Fields.WWNorton.ConceptsField = Alpaca.Fields.SelectField.extend(
    {
        setup: function () {

            var self = this;
            
            this.base();
            
            if (!self.options.dataSource) 
            {
                self.options.dataSource = {
                    "connector": true,
                    "config": {
                        "query": {
                            "_type": "test:chapter"
                        },
                        "mappings": {
                            "value": "_doc",
                            "text": "title"
                        }
                    }
                };
            }            
        }

    });

    Alpaca.registerFieldClass("concepts", Alpaca.Fields.WWNorton.ConceptsField);

});
