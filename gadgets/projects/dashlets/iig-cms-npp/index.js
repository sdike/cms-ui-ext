define(function(require, exports, module) {

    var html = require("text!./index.html");

    var Ratchet = require("ratchet/web");
    var OneTeam = require("oneteam");

    return Ratchet.DashletRegistry.register("iig-cms-npp", Ratchet.AbstractDashlet.extend({

        TEMPLATE: html,

        /**
         * @override
         */
        prepareModel: function(el, model, callback) {
            var self = this;
            // get the current project
            var project = this.observable("project").get();

            // the current branch
            var branch = this.observable("branch").get();
            
            var platform = this.observable("platform").get();
           
            //model.username = Gitana.readCookie('RATCHET_AUTH_USER_NAME');
            
            var authInfo = platform.getDriver().getAuthInfo();
            model.username = authInfo.getPrincipalName();
            model.accessToken = authInfo.accessToken;
            //model.repoId = branch.getRepositoryId();


            //model.branchId = branch._doc;

/*

authInfo.accessToken
branch._doc
branch.getRepositoryId()
 */

             
            this.base(el, model, function () {
                
                // TODO: add anything to the model
                // TODO: the model provides variables for the HTML template

               /* branch.queryNodes({ "_type": "nortoncore:site-version" }).then(function() {

                    // all of the products
                    model.sites = this.asArray(); 

                    // add "imageUrl" value to product (retrieve preview of width 256)
                    
    
                     for (var i = 0; i < model.sites.length; i++)
                    {
                        var site = model.sites[i];
                        site.browseUrl = "/#/projects/repository/" + site.getRepositoryId() + "/branch/" + site.getBranchId();
                        site.title = site.title;
                        
                    }

                    
                    
                    
                });*/
                callback();
            });
        },

        /**
         * @override
         */
        afterSwap: function(el, model, context, callback)
        {
/*            var self = this;


            this.base(el, model, context, function() {

                // TODO: grab any injected DOM elements and bind JS behaviors if needed
                // TODO: here is an example
                $(el).find(".testbutton").off().click(function(e) {
                    e.preventDefault();
                    
                    self.handleClickTestButton();
                })

                callback();

            });
            */
        },

        /** Called when the test button is clicked **/
        handleClickTestButton: function()
        {
            var self = this;
            
            alert("the test button was clicked");
        }
        
    }));
});
