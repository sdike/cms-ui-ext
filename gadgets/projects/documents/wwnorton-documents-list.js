define(function(require, exports, module) {

    require("css!./wwnorton-documents-list.css");

    var Ratchet = require("ratchet/web");
    var DocumentsList = require("app/gadgets/project/documents/documents-list");
    var OneTeam = require("oneteam");
    var bundle = Ratchet.Messages.using();

    return Ratchet.GadgetRegistry.register("wwnorton-documents-list", DocumentsList.extend({

        doGitanaQuery: function(context, model, searchTerm, query, pagination, callback)
        {
            var path = model.path;

            OneTeam.projectNode(this, "root", path, function() {

                if (!pagination.sort)
                {
                    pagination.sort = {};
                    pagination.sort["title"] = 1;
                    pagination.sort["_features.f:container.enabled"] = 1;
                }

                if (OneTeam.isEmptyOrNonExistent(query) && searchTerm)
                {
                    query = OneTeam.searchQuery(searchTerm, ["title", "description"]);
                }

                if (query.title) {
                    query.title = {
                        "$and": [
                            {"$eq": query.title},
                            {"$nin": ["System", "Definitions", "Applications", "Templates"]}
                        ]
                    };
                } else {
                    query.title = {
                        "$nin": ["System", "Definitions", "Applications", "Templates"]
                    };
                }

                var config = {
                    "type": "a:child",
                    "direction": "OUTGOING"
                };

                this.queryRelatives(query, config, pagination).then(function() {
                    this.then(function() {
                        callback(this);
                    });
                });

            });
        }

    }));

});
