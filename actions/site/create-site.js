define(function(require, exports, module) {

    var Ratchet = require("ratchet/ratchet");
    var Actions = require("ratchet/actions");
    var OneTeam = require("oneteam");
    var $ = require("jquery");

    return Ratchet.Actions.register("create-site", Ratchet.AbstractUIAction.extend({

        defaultConfiguration: function()
        {
            var config = this.base();

            config.title = "Create Site";
            config.iconClass = "fa fa-file-text";

            return config;
        },

        execute: function(config, actionContext, callback)
        {
            this.doAction(actionContext, function(err, result) {
                callback(err, result);
            });
        },

        doAction: function(actionContext, callback)
        {
            var self = this;

            // modal dialog
            Ratchet.fadeModal({
                "title": "Create a Site",
                "cancel": true
            }, function(div, renderCallback) {

                // append the "Create" button
                $(div).find('.modal-footer').append("<button class='btn btn-primary pull-right create'>Create</button>");

                // body
                $(div).find(".modal-body").html("");
                $(div).find(".modal-body").append("<div class='form'></div>");

                // form definition
                var c = {
                    "data": {
                    },
                    "schema": {
                        "type": "object",
                        "properties": {
                            "title": {
                                "type": "string",
                                "required": true
                            },
                            "description": {
                                "type": "string"
                            },
                            "siteId": {
                                "type": "string",
                                "required": true
                            }
                        }
                    },
                    "options": {
                        "fields": {
                            "title": {
                                "type": "text",
                                "label": "Title"
                            },
                            "description": {
                                "type": "textarea",
                                "label": "Description"
                            },
                            "siteId": {
                                "type": "text"
                            }
                        },
                        "focus": "title"
                    }
                };

                c.postRender = function(control)
                {
                    OneTeam.bindFormChildEnterClick(control, $(div).find(".create"));

                    // create button
                    $(div).find('.create').click(function() {

                        OneTeam.processFormAction(control, div, function(object) {
                            
                            // create site
                            self.block("Creating your site...", function() {
                                
                                self.createSite(actionContext, control.getValue(), function() {

                                    self.unblock(function() {
                                        callback();
                                    });
                                    
                                });
                            });
                        });

                    });

                    renderCallback(function() {
                        // TODO: anything?
                    });
                };

                var _form = $(div).find(".form");
                OneTeam.formCreate(_form, c);
            });
        },
        
        createSite: function(actionContext, form, callback)
        {
            var title = form.title;
            var description = form.description;
            var siteId = form.siteId;
            
            // TODO: how 
            OneTeam.projectBranch(actionContext, function() {
                
                // add top level folder
                self.addFolder(actionContext, title, { description: description, siteId: siteId }, "/Sites", function() {
                    
                    // add a sub-folder
                    self.addFolder(actionContext, "SubFolder", {}, "/Sites/" + title, function() {
                        
                        // all done
                        callback();
                    });                    
                });
            });
        },
        
        addFolder: function(actionContext, title, config, parentPath, callback)
        {
            OneTeam.projectBranch(actionContext, function() {
                
                var obj = {};
                if (config) {
                    obj = JSON.parse(JSON.stringify(config));
                }
                obj.title = title;
                this.createNode(obj, {
                    "rootNodeId": "root",
                    "parentFolderPath": parentPath,
                    "associationType": "a:child"
                }).then(function() {
                    callback();
                });                
            });            
        }

    }));
});

