# wwnorton-uiext-example



Requirements

WW Norton ref: http://applications.wwnorton.com:8080/browse/IIGNEW-29

        1. Remove all the Dev projects from Editors Platform View.
        2. Hide the non-content folders, e.g. Definitions, Templates from Editors on Documents View.

https://wwnorton.cloudcms.net/#/projects/1c8ce3fb1bc06352b25c/documents
editor/Seagull2015

(NOTE: assuming these are for "editor" role)

Additional comments:

        3. On Document Library, we want to hide all the system folders from editors but only show the content folder.
        4. On the Action Create Document, only shows the content types the users have access to on the dropdown list. 
           All the default content types should be hidden.
        5. When Create New Document, can we have the new document open in a full window? Like when in view property?
DONE    6. Since we are not using Data Lists, can we hide it?

Project Dashboard:

DONE    7. Remove Recent Project Activities
DONE    8. Remove Recent Project Applications (already removed by main UI)
DONE    9. Customize Getting Started with Your Projects Module
DONE    10. Remove Web Pages and Web Templates
        11. Remove all the system Tags on Tags view (TODO: what are these?)

Detail View:

DONE    12. Only keep the following - Overview, Actions, Attachments,Comments,Tags, Versions, Workflows, Properties
DONE    13. Actions - Remove Change Type, Change Name, Touch Document